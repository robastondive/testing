package re.xes.example;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

public class MyService extends Service {
    public static final String START_SERVICE_NOW_INTENT = "re.ve.START";

    public static final String STARTING_CODE_EXTRA = "startingCode";
    public int code = 0;
    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(@Nullable Intent intent,int flags,int startId){
        super.onStartCommand(intent,flags,startId);
        final String action = intent.getAction();
        if(START_SERVICE_NOW_INTENT.contentEquals(action)){
            code=intent.getIntExtra(STARTING_CODE_EXTRA,0);
        }
        return 1;
    }
}
