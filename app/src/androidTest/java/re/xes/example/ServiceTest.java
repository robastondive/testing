package re.xes.example;

import android.content.Intent;
import android.test.ServiceTestCase;

import re.xes.example.MyService;

/**
 * Created by rob on 19/07/2015.
 */
public class ServiceTest extends ServiceTestCase<MyService> {
    /**
     * Constructor
     *
     * @param serviceClass The type of the service under test.
     */
    public ServiceTest(Class<MyService> serviceClass) {
        super(serviceClass);
    }

    public void testStartService(){
        Intent startingIntent = new Intent(MyService.START_SERVICE_NOW_INTENT);
        startingIntent.setPackage(getContext().getPackageName());
        startingIntent.putExtra(MyService.STARTING_CODE_EXTRA, 1);
        startService(startingIntent);
        final MyService myService = getService();
        assertNotNull(myService);
    }
}
